﻿using System;

namespace Task1And2Experis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello!! I am Emil");
            Console.WriteLine("What is your name?");
            Console.WriteLine("Please enter below: ");

            string name = Console.ReadLine();


            int lengthOfName = name.Length;
            string firstLetterOfName = name.Substring(0, 1);

            Console.WriteLine("Hello " + name + "! Your name is "
                + lengthOfName + " characters long and starts with an " + firstLetterOfName + ".");


        }
    }
}
